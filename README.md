ansible-role-loki
=========

Create Loki config files. Use in combination with [this Loki docker setup](https://gitlab.com/naturalis/core/analytics/docker-compose-loki).

Requirements
------------
* Ansible >= 2.7



Role Variables
--------------

| Name           | Default value | Description                        |
| -------------- | ------------- | -----------------------------------|
| `loki_config_dir` | '/opt/compose_projects/loki/config' | Config directory. |

Dependencies
------------

* https://gitlab.com/naturalis/core/analytics/docker-compose-loki


Example Playbook
----------------

```yaml
- name: Apply loki role
  include_role:
    name: loki
    apply:
      tags: loki
  tags: always
```

License
-------
Apache License 2.0

Author Information
----------------
* Rudi Broekhuizen
